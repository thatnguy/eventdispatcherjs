;(function(root, document, factory, undefined) {
	'use strict';
	
	// use AMD if have support.
	if (typeof define === 'function' && define.amd) {

		// wrapping our factory for AMD loader.
		// and passing the return value to `root` (window).
		define(['lib/utils'], function() {
			return (root.EventDispatcher = factory(utils));
		});

	} else {
		// browser globals.
		root.EventDispatcher = factory(root.utils);
	}
})(this, document, function(utils) {

	// Simple Event Dispatcher 
	return function() {

		// binded events
		var events = {};

		var self = this;

		function fire(name) {
			var args = arguments, slice = Array.prototype.slice;

			if (arguments.length < 1) {
				return false;
			}
			
			args = slice.call(arguments, 1);

			var callbacks = events[name];

			if (!callbacks) {
				return false;
			}

			for (var i = 0, l = callbacks.length; i < l; i++) {
				callbacks[i].apply(this, args);
			}
		}

		function on(name, callback) {

			callback = typeof callback !== 'undefined'
					 ? callback
					 : function() { return false; };

			// callback needs to be a function
			if (!utils.isFunction(callback)) {
				return false;
			}

			var names = name.split(' '), l = names.length, n, ev;

			while (l--) {
				n = names[l];

				if (!utils.isEmpty(n)) {
					ev = events[n];

					// no events for this name was binded.
					if (!ev) {
						ev = events[n] = [];
					}

					ev.push(callback);
				}

			}

			return self;
		}

		function off(name) {
			if (has(name)) {
				return delete events[name];
			}

			return false;
		}

		function has(name) {
			return events[name] ? true : false;
		}

		// expose public functions.
		self.fire = fire;
		self.on   = on;
		self.off  = off;
		self.has  = has;
	}
});
